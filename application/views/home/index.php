<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
  <link rel="icon" href="img/microsoft_PNG9.png" type="image/png" sizes="16x16">
  <title>Micrepest</title>
</head>
<body>
  <div class="container">
    <div class="head">
      <h1>Get Your Passion here!</h1>
      <p>
        Happiness comes in waves. It’ll find you again.
      </p>
      <button>About Us</button>
      <button id="trans">Learn More</button>
    </div>
    
    <?php $this->load->view('layouts/header')?>
    
    <div class="card-container">
      <h1>Popular Post</h1>
      <div class="card-center">
        <div class="card">
          <img src="<?=base_url('assets/img/news.jpg')?>">
          <div class="card-ctn">
            <h3>Latest Post</h3>
            <p>
              Berita terbaru dan update
            </p>
            <div class="post-meta">
              <span><i class="fas fa-clock"></i> 6 mins ago</span>
              <span><i class="fas fa-comments"></i> 39 comments</span>
            </div>
          </div>
        </div>
        <div class="card">
          <img src="<?=base_url('assets/img/news.jpg')?>">
          <div class="card-ctn">
            <h3>Latest Post</h3>
            <p>
              Berita terbaru dan update
            </p>
            <div class="post-meta">
              <span><i class="fas fa-clock"></i> 6 mins ago</span>
              <span><i class="fas fa-comments"></i> 39 comments</span>
            </div>
          </div>
        </div>
        <div class="card">
          <img src="<?=base_url('assets/img/news.jpg')?>">
          <div class="card-ctn">
            <h3>Latest Post</h3>
            <p>
              Berita terbaru dan update
            </p>
            <div class="post-meta">
              <span><i class="fas fa-clock"></i> 6 mins ago</span>
              <span><i class="fas fa-comments"></i> 39 comments</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="article-container">
      <div class="post-item">
        <div class="article-item">
          <div class="article-prev">
            <h2>
              <a href="<?=site_url('page/index/article')?>">
                [Flash] Shopee Klaim Telah Berhasil Raih Lisensi E-money untuk Layanan ShopeePay
              </a>
            </h2>
            <p>
              [Flash] Shopee Klaim Telah Berhasil Raih Lisensi E-money untuk Layanan ShopeePay Pada 2017 lalu, Bank Indonesia sempat meminta ShopeePay, TokoCash, BukaDompet, hingga GrabPay, untuk menghentikan layanan pengisian saldo masing-masing..
            </p>
            <br>
            <hr>
            <br>
            <span class="article-meta">Posted By: <i class="fas fa-user"></i> admin1</span>
            <span class="article-meta"><i class="fas fa-clock"></i> 6 mins ago</span>
            <span class="article-meta comment"><i class="fas fa-comment"></i> 1 Comment</span>
          </div>
          <div class="article-img">
            <img src="<?=base_url('assets/img/IMG_20180309_151110-768x432.jpg')?>">
          </div>
        </div>
      </div>
      <div class="post-item">
        <div class="article-item">
          <div class="article-prev">
            <h2>
              <a href="#">
                [Flash] Shopee Klaim Telah Berhasil Raih Lisensi E-money untuk Layanan ShopeePay
              </a>
            </h2>
            <p>
              [Flash] Shopee Klaim Telah Berhasil Raih Lisensi E-money untuk Layanan ShopeePay Pada 2017 lalu, Bank Indonesia sempat meminta ShopeePay, TokoCash, BukaDompet, hingga GrabPay, untuk menghentikan layanan pengisian saldo masing-masing..
            </p>
            <br>
            <hr>
            <br>
            <span class="article-meta">Posted By: <i class="fas fa-user"></i> admin1</span>
            <span class="article-meta"><i class="fas fa-clock"></i> 6 mins ago</span>
          </div>
          <div class="article-img">
            <img src="<?=base_url('assets/img/IMG_20180309_151110-768x432.jpg')?>">
          </div>
        </div>
      </div>
      <div class="post-item">
        <div class="article-item">
          <div class="article-prev">
            <h2>
              <a href="#">
                [Flash] Shopee Klaim Telah Berhasil Raih Lisensi E-money untuk Layanan ShopeePay
              </a>
            </h2>
            <p>
              [Flash] Shopee Klaim Telah Berhasil Raih Lisensi E-money untuk Layanan ShopeePay Pada 2017 lalu, Bank Indonesia sempat meminta ShopeePay, TokoCash, BukaDompet, hingga GrabPay, untuk menghentikan layanan pengisian saldo masing-masing..
            </p>
            <br>
            <hr>
            <br>
            <span class="article-meta">Posted By: <i class="fas fa-user"></i> admin1</span>
            <span class="article-meta"><i class="fas fa-clock"></i> 6 mins ago</span>
          </div>
          <div class="article-img">
            <img src="<?=base_url('assets/img/IMG_20180309_151110-768x432.jpg')?>">
          </div>
        </div>
      </div>  
    </div>
    <?php $this->load->view('layouts/footer')?>