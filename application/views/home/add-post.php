<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
  <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">
  <link rel="icon" href="img/microsoft_PNG9.png" type="image/png" sizes="16x16">
  <title>Admin Post</title>
</head>
<body>
  <div class="container">
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="adm-sidebar">
      <h1>Admin Page</h1>
      <div class="menu">
        <ul>
          <li><i class="fas fa-tachometer-alt"><a href="<?=site_url('page/index/admin-dash')?>"></i> Dashboard</a></li>
          <li class="active"><i class="fas fa-list-ul"></i><a href="<?=site_url('page/index/post-list')?>"> Post List</a></li>
          <li><i class="fas fa-user"></i><a href="#"> Profile</a></li>
          <li><a href="index.html"><i class="fas fa-power-off"></i> Log Out</a></li>
        </ul>
      </div>
    </div>
    <div class="adm-top-nav">
      <div class="top-nav-item">
        <div class="nav-prof">
          <span>
            <a href="#">Panda</a>
            <img src="<?=base_url('assets/img/panda-emoji-png-2.png')?>">
          </span>
        </div>
        <form class="search-box"action="#">
          <input type="text" name="search">
          <button><i class="fas fa-search"></i></button>
        </form>
      </div>
    </div>
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="adm-content">
      <div class="adm-header">
        <h3>Add New</h3>
      </div>
      <div class="new-container">
        <h1>Add New Post</h1>
        <div class="add-container">
          <input type="text" name="title" placeholder="Enter title here">
          <div class="toolbar">
            <button><i class="fas fa-bold"></i></button>
            <button><i class="fas fa-italic"></i></button>
            <button><i class="fas fa-strikethrough"></i></button>
            <button><i class="fas fa-list-ul"></i></button>
            <button><i class="fas fa-list-ol"></i></button>
            <button><i class="fas fa-quote-right"></i></button>
            <button><i class="fas fa-align-left"></i></button>
            <button><i class="fas fa-align-right"></i></button>
            <button><i class="fas fa-align-justify"></i></button>
            <button><i class="fas fa-align-center"></i></button>
            <button><i class="fas fa-link"></i></button>
            <button><i class="fas fa-unlink"></i></button>
          </div>
          <textarea name="desc" cols="70" rows="20"></textarea>
          <div class="tags">
            <h3>Tags</h3>
            <input type="text" name="tags">
            <button class="btn-update">Add</button>
            <i>
              Gunakan koma sebagai pemisah
            </i>
          </div>        
        </div>
        <div class="new-1">
            <div class="tags publish">
              <h3>Publish</h3>
              <button>Save Draft</button>
              <button>Preview</button>
              <ul>
                <li><a class="fas fa-key"><span> Status : </span><b>Draft</b></a></li>
                <li><a class="fas fa-eye"><span> Visibility :</span> <b> Public</b></a></li>
              </ul>
              <a href="<?=site_url('page/index/post-list')?>"><button id="btn-pls">Publish</button></a>
            </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>