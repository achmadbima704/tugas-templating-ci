<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
  <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">
  <link rel="icon" href="<?=base_url('assets/img/microsoft_PNG9.png')?>" type="image/png" sizes="16x16">
  <title>Admin Dashboard</title>
</head>
<body>
  <div class="container">
    <div class="adm-sidebar">
      <h1>Admin Page</h1>
      <div class="menu">
        <ul>
          <li class="active"><i class="fas fa-tachometer-alt"><a href="#"></i> Dashboard</a></li>
          <li><i class="fas fa-list-ul"></i><a href="<?=site_url('page/index/post-list')?>"> Post List</a></li>
          <li><i class="fas fa-user"></i><a href="#"> Profile</a></li>
          <li><a href="<?=site_url('home')?>"><i class="fas fa-power-off"></i> Log Out</a></li>
        </ul>
      </div>
    </div>
    <div class="adm-top-nav">
      <div class="top-nav-item">
        <div class="nav-prof">
          <span>
            <a href="#">Panda</a>
            <img src="<?=base_url('assets/img/panda-emoji-png-2.png')?>">
          </span>
        </div>
        <form class="search-box"action="#">
          <input type="text" name="search">
          <button><i class="fas fa-search"></i></button>
        </form>
      </div>
    </div>
    <div class="adm-content">
      <div class="adm-header">
        <h3>Dashboard</h3>
      </div>
      
      <div class="col-container">
        <div class="col">
          <h3>Total Views</h3>
          <span>642</span>
        </div>
        <div class="col">
          <h3>Total Page Views</h3>
          <span>642</span>
        </div>
        <div class="col">
          <h3>Total Views This Month</h3>
          <span>642</span>
        </div>
      </div>

      <div class="table-container">
        <h3>Latest Post</h3>
        <table class="table">
          <tr>
            <td>No</td>
            <td>Title</td>
            <td>Category</td>
            <td>Description</td>
            <td>Date Post</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Lorem</td>
            <td>Ipsum</td>
            <td>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam, distinctio deserunt, exercitationem culpa quam cupiditate hic incidunt excepturi fugiat magnam nostrum tenetur vero inventore error impedit quos, id assumenda eaque.</p>
            </td>
            <td>23-04-2018</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Lorem</td>
            <td>Ipsum</td>
            <td>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam, distinctio deserunt, exercitationem culpa quam cupiditate hic incidunt excepturi fugiat magnam nostrum tenetur vero inventore error impedit quos, id assumenda eaque.</p>
            </td>
            <td>23-04-2018</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Lorem</td>
            <td>Ipsum</td>
            <td>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam, distinctio deserunt, exercitationem culpa quam cupiditate hic incidunt excepturi fugiat magnam nostrum tenetur vero inventore error impedit quos, id assumenda eaque.</p>
            </td>
            <td>23-04-2018</td>
          </tr>
        </table>
      </div>
      
    </div>
  </div>
</body>
</html>