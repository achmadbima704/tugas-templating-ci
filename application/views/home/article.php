<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
  <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">
  <link rel="icon" href="img/microsoft_PNG9.png" type="image/png" sizes="16x16">
  <title>Micrepest</title>
</head>
<body>
  <div class="container">
    <?php $this->load->view('layouts/header');?>
    <div class="article-container">
      <div class="article-body">
        <h1>
          [Flash] Shopee Klaim Telah Berhasil Raih Lisensi E-money untuk Layanan ShopeePay
        </h1>
        <div class="article-meta">
          <div class="author"></div>
          <div class="det-author">
            <span>Joker</span>
            <span>3:00 PM on Sep 27, 2018</span>
          </div>
        </div>
        <img src="<?=base_url('assets/img/IMG_20180309_151110-768x432.jpg')?>">
        <div class="main">
          <h2>Inti Berita</h2>
          <ul>
            <li>
              Pada akhir September 2018, Country Brand Manager Shopee Indonesia Rezki Yanuar mengaku bahwa pihaknya telah mendapat lisensi uang elektronik untuk layanan pembayaran ShopeePay mereka.
            </li>
            <li>
              Kabar tersebut mereka terima dari Bank Indonesia beberapa minggu lalu.
            </li>      
            <li>
              Lisensi tersebut diberikan kepada perusahaan afiliasi mereka, yaitu PT Airpay Internasional Indonesia.
            </li>      
            <li>
              Saat ini, layanan ShopeePay masih dalam tahap pengembangan.
            </li>
          </ul>  
        </div>
        <div class="main">
          <h2>Fakta penting lainnya</h2>
          <ul>
            <li>
              Pada 2017 lalu, Bank Indonesia sempat meminta para penyedia layanan uang elektronik yang belum berizin, seperti TokoCash milik Tokopedia, BukaDompet milik Bukalapak, ShopeePay milik Shopee, GrabPay milik Grab, serta Paytren, untuk menghentikan layanan isi ulang saldo.
            </li>
            <li>
              Karena tidak kunjung mendapat lisensi, beberapa pihak yang terdampak memutuskan untuk bekerja sama dengan perusahaan lain yang telah mempunyai lisensi, seperti yang dilakukan Grab dengan menggaet OVO, serta Bukalapak yang menggandeng DANA.
            </li>
            <li>
              Salah satu layanan uang elektronik milik startup yang tidak sampai mengalami hal tersebut adalah GO-PAY milik GO-JEK, yang memang telah mengakuisisi sebuah startup pemilik lisensi e-money bernama MVCommerce pada pada tahun 2016 silam.
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="card-container">
      <h1>You may also like</h1>
      <div class="card-center">
        <div class="card">
          <img src="<?=base_url('assets/img/news.jpg')?>">
          <div class="card-ctn">
            <h3>Latest Post</h3>
            <p>
              Berita terbaru dan update
            </p>
            <div class="post-meta">
              <span><i class="fas fa-clock"></i> 6 mins ago</span>
              <span><i class="fas fa-comments"></i> 39 comments</span>
            </div>
          </div>
        </div>
        <div class="card">
          <img src="<?=base_url('assets/img/news.jpg')?>">
          <div class="card-ctn">
            <h3>Latest Post</h3>
            <p>
              Berita terbaru dan update
            </p>
            <div class="post-meta">
              <span><i class="fas fa-clock"></i> 6 mins ago</span>
              <span><i class="fas fa-comments"></i> 39 comments</span>
            </div>
          </div>
        </div>
        <div class="card">
          <img src="<?=base_url('assets/img/news.jpg')?>">
          <div class="card-ctn">
            <h3>Latest Post</h3>
            <p>
              Berita terbaru dan update
            </p>
            <div class="post-meta">
              <span><i class="fas fa-clock"></i> 6 mins ago</span>
              <span><i class="fas fa-comments"></i> 39 comments</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('layouts/footer');?>