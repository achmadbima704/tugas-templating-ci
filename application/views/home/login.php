<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">
  <link rel="icon" href="<?=base_url('assets/img/microsoft_PNG9.png')?>" type="image/png" sizes="16x16">
  <title>Login Page</title>
</head>
<body>
  <div class="lgn-container">
    <div class="bg-login">
      <img src="<?=base_url('assets/img/avi-richards-183715-unsplash.jpg')?>" alt="">
    </div>
    <div class="form-container">
      <form class="form-body" action="<?=base_url('page/index/admin-dash')?>" method="post">
        <h1>MyBlog Admin</h1>
        <h3>Log in</h3>
        <input type="text" name="username" placeholder="Username" required>
        <input type="password" name="password" placeholder="Password" required>
        <button type="submit">Log In</button>
        <span><input type="checkbox" name="remember"> Remember me |</span>
        <span><a href="#">Lupa Password anda?</a></span>
        <span id="home"><a href="<?=site_url('home')?>">Beranda</a></span> 
      </form>
    </div>
  </div>
</body>
</html>