<footer>
      <div class="footer-container">
        <div class="footer-item">
          <div class="footer-info">
            <h3>INFORMATION</h3>
            <ul>
              <li><a href="#">About</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">Services</a></li>
            </ul>
          </div>
          <div class="footer-info">
            <h3>ACCOUNT</h3>
            <ul>
              <li><a href="#">Login</a></li>
              <li><a href="#">Register</a></li>
              <li><a href="#">Forgot Password</a></li>      
            </ul>
          </div>
        </div>
        <div class="footer-item newsletter">
          <h3>SIGN UP FOR OUR NEW LETTER</h3>
          <p>
            Kebijakan privasi berlaku untuk ini.
          </p>
          <input type="text" name="email">
          <button>SUBMIT</button>
        </div>
      </div>
    </footer>
    <span class="copy">&copy; 2018 <span id="name">Achmadbima</span></span>
  </div>
</body>
</html>