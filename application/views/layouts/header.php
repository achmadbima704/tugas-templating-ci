  <nav class="nav">
    <ul class="nav-list nav-item">
      <li id="brand">
        <a href="#">
          <span><i class="fab fa-microsoft"></i> Micrepest</span>
        </a>
      </li>
      <li><a href="#">Home</a></li>
      <li><a href="#">About</a></li>
      <li><a href="#">Services</a></li>
    </ul>
    <ul class="nav-list lgn-btn nav-item">
      <li><a href="<?=site_url('page/index/login')?>"><i class="fas fa-sign-in-alt"></i> Login</a></li>
    </ul>
  </nav>