<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

  public function index()
  {
    
    $page = $this->uri->segment(3);

    $this->load->view('home/'.$page);
    
  }

}

/* End of file Page.php */
